// pages/subpackage/pages/extend/contractList/contractList.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',//识别id
    dataInfo: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id: options.id || '',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getDataList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  //获取合同文件列表
  getDataList() {
    var that = this;
    app.ajax({
      url: 'User/Work/getEnergySavingDataList',
      method: "POST",
      data: {
        identification_id: that.data.id,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            dataInfo: res.data.data
          })
        }
      }
    })
  },
  // 跳转查看合同
  lookContract(e){
    var that = this;
    wx.navigateTo({
      url: '../lookContract/lookContract?id=' + that.data.id,
    })
  },
  // 跳转数据采集详情
  lookDataAcquisition(e) {
    var that = this;
    var id = that.data.id;
    var type = e.currentTarget.dataset.type;//1、测试确认单、2保管清单、3分享确认单
    var work_order_type = e.currentTarget.dataset.work_order_type;//1试用2改造
    wx.navigateTo({
      url: '../orderConfirmInfo/orderConfirmInfo?id=' + id + '&type=' + type + '&work_order_type=' + work_order_type,
    })
  },
})