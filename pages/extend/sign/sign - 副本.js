// pages/ceshi/ceshi.js
var app = getApp();
var content = null;
var touchs = [];
var canvasw = 0;
var canvash = 0;
var isdraw = 0;//是否画的长度超过10否者不能提交
//获取系统信息
wx.getSystemInfo({
  success: function (res) {
    canvasw = res.windowWidth;
    canvash=res.windowHeight;
    console.log('结果>>',res)
  },
});
let falg = false;//提交防抖

  Page({
    /**
    * 页面的初始数据
    */
    data: {
      on_it:true,
      contract_id:'',
      knowShow: true,
      user_id:'',
      canvasws:0,
      canvashs:0,
      realname:"",//租赁人真实名字
      work_order_id:"",
    },
    // 画布的触摸移动开始手势响应
    start: function (event) {
      // console.log("触摸开始" + event.changedTouches[0].x)
      // console.log("触摸开始" + event.changedTouches[0].y)
      //获取触摸开始的 x,y
      let point = { x: event.changedTouches[0].x, y: event.changedTouches[0].y }
      touchs.push(point)
    },

    // 画布的触摸移动手势响应
    move: function (e) {
      let point = { x: e.touches[0].x, y: e.touches[0].y }
      touchs.push(point)
      if (touchs.length >= 2) {
        this.draw(touchs);
        isdraw += 1
      }
      console.log('长度', isdraw)
    },

    // 画布的触摸移动结束手势响应
    end: function (e) {
      console.log("触摸结束" + e)
      //清空轨迹数组
      for (let i = 0; i < touchs.length; i++) {
        touchs.pop()
      }

    },

    // 画布的触摸取消响应
    cancel: function (e) {
      console.log("触摸取消" + e)
    },

    // 画布的长按手势响应
    tap: function (e) {
      console.log("长按手势" + e)
    },

    error: function (e) {
      console.log("画布触摸错误" + e)
    },
    /**
    * 生命周期函数--监听页面加载
    */
    onLoad: function (options) {
      var contract_id = options.contract_id;
      var that = this;
      wx.getSystemInfo({
        success: function (res) {
          canvasw = res.windowWidth;
          canvash = res.windowHeight;
          console.log('结果>>', res);
          that.setData({
            canvasws: res.windowWidth,
            canvashs: res.windowHeight
          })
        },
      }),
      that.setData({
        contract_id: contract_id,
        user_id:options.user_id,
        realname:options.realname,
        work_order_id: options.work_order_id
      })
    },

    /**
    * 生命周期函数--监听页面初次渲染完成
    */
    onReady: function () {
    },
    onShow:function(){
      
      var that = this;
      falg = false;
      //获得Canvas的上下文
      content = wx.createCanvasContext('firstCanvas')
      that.clearClick();
      //设置线的颜色
      content.strokeStyle = '#191919'
      //设置线的宽度
      content.lineWidth = 4
      //设置线两端端点样式更加圆润
      content.lineCap = 'round';
      //设置两条线连接处更加圆润
      content.lineJoin = 'round';
    },
    //绘制
    draw: function (touchs) {
      let point1 = touchs[0]
      let point2 = touchs[1]
      touchs.shift()
      content.moveTo(point1.x, point1.y)
      content.lineTo(point2.x, point2.y)
      content.stroke();
      content.draw(true)
    },
    //清除操作
    clearClick: function () {
      //清除画布
      var that=this;
      content.clearRect(0, 0, canvasw, canvash);
      content.draw(true);
    },
    //保存图片
    saveClick: function () {
      var that = this;
      wx.showModal({
        title: '提示',
        content: '是否确认提交签名?必须是租赁本人'+that.data.realname+"的签名",
        success(res) {
          if (res.confirm) {
            if (falg == true) {
              return false;
            }
            if (isdraw <= 10) {
              app.showToast('请签写正确签名');
              return false;
            }
            falg = true;
            // wx.showLoading({
            //   title: '签名提交中',
            // })
            content = wx.createCanvasContext('firstCanvas')
            wx.canvasToTempFilePath({
              canvasId: 'firstCanvas',
              success: function (res) {
                //打印图片路径
                console.log(res.tempFilePath)
                //设置保存的图片
                wx.navigateTo({
                  url: '../econtractAudit/econtractAudit?signImage=' + res.tempFilePath + '&contract_id=' + that.data.contract_id + '&user_id=' + that.data.user_id + "&work_order_id=" + that.data.work_order_id,
                })
                // that.fillPdf(res.tempFilePath);
              }
            })
          }
          if(res.cancel){

          }
        }
      })
      
        
    },
    //打开预览PDF
  preview: function (filename){
          wx.downloadFile({
            // 示例 url，并非真实存在
            url: app.globalData._url + filename,
            success: function (res) {
              const filePath = res.tempFilePath
              wx.openDocument({
                filePath: filePath,
                success: function (res) {
                  console.log('打开文档成功')
                }
              })
            }
          })
         
    },
    //填充PDF数据
  fillPdf: function (tempFilePath){
      var that = this;
      wx.uploadFile({
        url: app.globalData._url + 'Common/Esignc/pdf',
        filePath: tempFilePath,
        header: {
          company: app.globalData.company_id,
          client: app.globalData.client,
        },
        name: 'file',
        formData:{
          'contract_id': that.data.contract_id
        },
        success: function (res) {
          wx.hideLoading();
          var data = JSON.parse(res.data);
          if (data.code == 1000) {
            console.log(data.data.filename)
            that.signContract(data.data.filename, data.data.esign_user_sign);
          } else {
            falg = false;
            app.showToast(data.message);
          }
        },
        fail: function (err) {
          console.log(err);
        }

      })
    },
    //签合同
  signContract: function (filename, esign_user_sign){
      var that = this;
      app.ajax({
        url: 'Engineer/Contract/sign',
        data: {
          contract_id: that.data.contract_id,
          contact_type: 2,
          esign_filename:filename,
          esign_user_sign: esign_user_sign,
          sign_client: 2,
          work_order_id: that.data.work_order_id,
          user_id: that.data.user_id
        },
        success: function (res) {
          wx.hideLoading();
          if (res.data.code == 1000) {
            app.showToast(res.data.message);
            setTimeout(function(){
              wx.navigateBack({
                delta: 1
              })
            },1000);
          } else {
            falg = false;
            app.showToast(res.data.message);
          }
        }
      })
    },
  //清除操作
  clearClick: function () {
    //清除画布
    var that = this;
    content.clearRect(0, 0, canvasw, canvash);
    content.draw(true);
    isdraw = 0;
  },
  colorChoose: function (e) {
    var that = this, on_it = that.data.on_it;
    that.clearClick();//清除
    if (on_it){
      that.setData({
        on_it: false
      })
      //获得Canvas的上下文
      content = wx.createCanvasContext('firstCanvas')
      //设置线的颜色
      content.strokeStyle = "#c80000"
      //设置线的宽度
      content.lineWidth = 4
      //设置线两端端点样式更加圆润
      content.setLineCap('round')
      //设置两条线连接处更加圆润
      content.setLineJoin('round')
    }else{
      that.setData({
        on_it: true
      })
      //获得Canvas的上下文
      content = wx.createCanvasContext('firstCanvas')
      //设置线的颜色
      content.strokeStyle = "#191919"
      //设置线的宽度
      content.lineWidth = 4
      //设置线两端端点样式更加圆润
      content.setLineCap('round')
      //设置两条线连接处更加圆润
      content.setLineJoin('round')
      console.log('红色???', content);
    }
    isdraw = 0;
  },
  // 点击知道了
  know: function () {
    var that = this;
    that.setData({
      knowShow: false
    })

  }

  })