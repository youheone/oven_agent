// pages/user/payRecord/payRecord.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
var orderInfo= [];
var load= true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderGroup:[],
    empty: true,
    hasMore: false,
    loading: false,
    load_show: false,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    _PAGE = 1;
    that.getOrderList();
    // that.test();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    _PAGE++;
    that.getOrderList();
  },
  // 获取付款记录列表
  getOrderList: function(){
    var that = this;
    app.ajax({
      load: load,
      msg: '加载中...',
      url: 'User/User/getBill',
      data:{
        page: _PAGE,
        pageSize: _PAGESIZE
      },
      success:function(res){
        if(res.data.code == 1000){
          if (_PAGE == 1) {
            that.setData({
              empty: false
            });
            orderInfo = res.data.data;
          } else {
            orderInfo = orderInfo.concat(res.data.data)
            that.setData({
              empty: false
            });
          }
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load= false;
          that.handleGroup(orderInfo);
          
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          load_show: true
        })
      }
    })

  },
  // 将数据处理分组
  handleGroup:function(arr){
    var that = this;
    var copyArr = arr;
    var dateArr = [];
    for (var i in arr) {
      dateArr.push(arr[i].format_date);
    }
    var dateArr2 = dateArr.filter(function (element, index, self) {
      return self.indexOf(element) === index;
    });

    var group = [];
    for (var i in dateArr2) {
      var data = [];
      for (var j in arr) {
        if (arr[j].format_date == dateArr2[i]) {
          data.push(arr[j]);
        }
      }
      group.push({
        date: dateArr2[i],
        data: data
      })
    }
    that.setData({
      orderGroup: group
    })
    console.log("group");
    console.log(group);

  },

  // 测试
  test: function () {
    var that = this;
    var arr = [
      {
        order_number: 'CQR920330844265458',
        format_time: '09:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月20日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '10:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月20日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '19:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月20日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '10:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月21日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '10:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月22日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '11:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月22日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '12:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月22日',
      },
      {
        order_number: 'CQR920330844265458',
        format_time: '13:21:04',
        pay_way: '二维码付款',
        money: '30.00',
        format_date: '2018年5月22日',
      },

    ];
    var dateArr = [];
    for (var i in arr) {
      dateArr.push(arr[i].format_date);
    }
    var dateArr2 = dateArr.filter(function (element, index, self) {
      return self.indexOf(element) === index;
    });
    // console.log(dateArr);
    // console.log(dateArr2);
    var group = [];
    for (var i in dateArr2) {
      var data = [];
      for (var j in arr) {
        if (arr[j].format_date == dateArr2[i]) {
          data.push(arr[j]);
        }
      }
      group.push({
        date: dateArr2[i],
        data: data
      })
    }
    that.setData({
      orderGroup: group
    })
    console.log(group);
  }
})