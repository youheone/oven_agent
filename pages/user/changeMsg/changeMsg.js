// pages/user/changeMsg/changeMsg.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    user_img: '',
    gender: 1, //1是男的  2是女的
    usermagInfo: '', //获取用户信息
    tjr:[],
    status: '',//是否认证，认证后不可修改昵称
    now_time: '', //开始年月
    last_time: '', //结束年月
    date:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // var that = this;
    // that.getUser_Info();
    // this.getInviter();
    this.getAuthInfo()

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    that.getUser_Info();
    that.getInviter();
  },
  editBirthday: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      date: date
    })
    app.ajax({
      url: 'User/User/editUserInfo',
      data: {
        birthday: date
      },
      success: function (res) {
        app.showToast(res.data.message);
        if(res.data.code==1000){
          var user_magInfo = that.data.usermagInfo;
          user_magInfo.birthday_update_num = user_magInfo.birthday_update_num+1;
          that.setData({
            usermagInfo: user_magInfo,
          })
        }
      }
    })
  },
  update() {
    const updateManager = wx.getUpdateManager()

    updateManager.onCheckForUpdate(function(res) {
      // 请求完新版本信息的回调
      if (!res.hasUpdate) {
        app.showToast("已经是最新版本");
        return false;
      }
    })

    updateManager.onUpdateReady(function() {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })

    updateManager.onUpdateFailed(function() {
      // 新版本下载失败
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  // 选择头像
  chooseimage: function() {
    var that = this;
    wx.showActionSheet({
      itemList: ['从相册中选择', '拍照'],
      itemColor: "#DF0101",
      success: function(res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {
            that.chooseWxImage('album')
          } else if (res.tapIndex == 1) {
            that.chooseWxImage('camera')
          }
        }
      }
    })
  },
  // 头像 上传路径
  chooseWxImage: function(type) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: [type],
      success: function(res) {
        wx.uploadFile({
          url: app.globalData._url + 'Common/Common/upload',
          filePath: res.tempFilePaths[0],
          name: 'file',
          success: function (result) {
            if (result.statusCode !== 200) {
              app.showToast('上传失败,请重新上传');
              return
            }
            var data = JSON.parse(result.data);
            if (data.code == 1000) {
              that.editUserImg(data.data.url, data.data.all_url); //调用修改接口
            } else {
              app.showToast("上传失败,请重新上传");
            }
          },
          fail: function (fail) {
            app.showToast('上传失败,请重新上传');
          }
        })
      }
    })
  },
  // 修改头像 调用接口............
  editUserImg(headimg, all_url) {
    var that = this;
    app.ajax({
      url: 'User/User/editUserInfo',
      data: {
        avatar_img: headimg
      },
      success: function(res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message);
          var user_magInfo = that.data.usermagInfo;
          user_magInfo.avatar_img_all = all_url;
          that.setData({
            usermagInfo: user_magInfo,
          })
        }

      }
    })
  },
  // 选择性别
  choosesex: function() {
    var that = this;
    wx.showActionSheet({
      itemList: ['男', '女'],
      itemColor: "#DF0101",
      success: function(res) {
        if (!res.cancel) {
          var gender_type = 0;
          if (res.tapIndex == 0) {
            gender_type = 1
          } else {
            gender_type = 2
          }
          app.ajax({
            url: 'User/User/editUserInfo',
            data: {
              gender: gender_type
            },
            success: function(res) {
              if (res.data.code == 1000) {
                app.showToast(res.data.message);
                var user_magInfo = that.data.usermagInfo;
                user_magInfo.gender = gender_type
                that.setData({
                  usermagInfo: user_magInfo
                })
              } else {
                app.showToast(res.data.message);
              }

            }
          })
        }
      }
    })
  },
  //修改名称
  changeName(e) {
    var that = this;
    var nickname = e.currentTarget.dataset.uname;
    if (that.data.status==2){
      return;
    }
    wx.navigateTo({
      url: '../changeName/changeName?nickname=' + nickname,
    })
  },
  //我的手机号码
  changePhone() {
    wx.navigateTo({
      url: '../changePhone/changePhone',
    })
  },
  //我的地地址列表
  myAddressList() {
    wx.navigateTo({
      url: '../myAddressList/myAddressList?on_way=' + 1,
    })
  },
  //我的发票
  invoiceList() {
    wx.navigateTo({
      url: '../invoiceList/invoiceList',
    })
  },
  //获取用户信息
  getUser_Info() {
    var that = this;
    app.ajax({
      url: "User/Spread/getInfo",
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            usermagInfo: res.data.data,
            date:res.data.data.birthday
          })
        } else {
          app.showToast(res.data.message, "none", 2000, function() {});
        }
      }
    })
  },
  //打开授权设置
  authorization() {
    var that = this;
    wx.openSetting({
      success(res) {
        // if (res.authSetting['scope.userLocation'] == true) {
        //    that.position();
        // }
      }
    })
  },

  // 退出登录
  signout() {
    wx.clearStorage({
      success() {
        wx.navigateTo({
          url: '../login/login',
        })
      }
    })
  },

  // 我的二维码
  qrcode(){
    wx.navigateTo({
      url: '/pages/subpackage/pages/extend/myQRcode/myQRcode',
    })
  },

  getInviter() {
    var that = this;
    app.ajax({
      url: 'User/Spread/Inviter',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            tjr:res.data.data
          })
        }
      }
    })
  },
  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "User/Realname/getAuthInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            status: res.data.data.status
          })
        } else {

        }
      }
    })
  }

})
