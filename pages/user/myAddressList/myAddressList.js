// pages/user/myAddressList/myAddressList.js
var app=getApp();
var pageSize = 7;//默认页量
var page_s = 1;//默认页码
var load= true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    choose_show: true,
    empty_show: false,
    address_list:[],//地址列表
    hasMore: false, //数据是否加载完成
    loading: false,
    on_way:'',
    load_show:false,//整体加一个开关 加载
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    that.setData({
      on_way:options.on_way
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this;
    that.setData({
      address_list: []
    })
    var page_s = 1;
    that.addressList(page_s);//获取地址列表
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    var that=this;
    that.setData({
      load_show: false
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;

    // 当没有数据时,不再请求
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    page_s += 1;
    that.addressList(page_s);

  },
  //修改地址
  editBind(e){
    var that = this;
    var address_id = e.currentTarget.dataset.id ? e.currentTarget.dataset.id:'';
    var Type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '../editAddress/editAddress?address_id=' + address_id+'&type='+Type,
    })
  },
  //返回费否需要返回地址
  backReturn(e){
    var that=this;
    var address_id = e.currentTarget.dataset.id;
    console.log('111111');
    console.log(address_id);
    // return;
    if (that.data.on_way==1){
        return false;
    }else{
      wx.setStorageSync('address_id', address_id);
      wx.navigateBack({
        delta: 1
      })
    }
  },
  //删除地址
  delBind(e){
    var that = this;
    var address_id = e.currentTarget.dataset.id;
    var indesx = e.currentTarget.dataset.indesx;
    wx.showModal({
      title: '提示',
      content: '是否删除该地址',
      confirmColor:'#DF0101',
      success: function (res) {
        if (res.confirm) {
          app.ajax({
            url: 'User/Useraddress/editAddress',
            method: "POST",
            data: { address_id: address_id, is_del: 1 },
            success: function (res) {
              console.log(res);
              if (res.data.code == 1000) {
                app.showToast('删除成功!', "none", 2000, function () { });
                var newlist = that.data.address_list;
                newlist.splice(indesx, 1);
                var address = wx.getStorageSync("address_id");
                if (address == address_id){
                  wx.removeStorageSync("address_id");
                }
                that.setData({
                  address_list: newlist
                })
              } else {
                app.showToast(res.data.message, "none", 2000, function () { });
              }
            }
          })
        } else if (res.cancel) {

        }
      }
    })
  },
  //单选事件
  radioChange: function (e) {
    var that=this;
    var address_id = e.currentTarget.dataset.id;
    app.ajax({
      url: 'User/Useraddress/editAddress',
      method: "POST",
      data: { address_id: address_id, is_default: 2 },
      success: function (res) {
          if(res.data.code==1000){
            app.showToast('修改成功!', "none", 2000, function () { });
          }else{
        app.showToast(res.data.message, "none", 2000, function () { });
      }
      }
    })
  },
  //获取地址列表
  addressList(page_s) {
    var that = this;
    app.ajax({
      url: 'User/Useraddress/addressList',
      method: "POST",
      load: load,
      msg: '加载中...',
      data: { page: page_s, pageSize: pageSize },
      success: function (res) {
        //是否需要加载更多
        var hasMore = true;
        if (res.data.data.length < pageSize) {
          hasMore = false;
        }
        that.setData({
          hasMore: hasMore,
          loading: false
        })
        if (res.data.code == 1000) {
          var changeList = "";
          if (that.data.address_list == "") {
            changeList = res.data.data;
          } else {
            var addList = that.data.address_list;
            changeList = addList.concat(res.data.data);
          }
          that.setData({
            address_list: changeList
          })
          load= false;
        }else{
         // app.showToast(res.data.message, "none", 2000, function () { });
        }
        that.setData({
          load_show:true,
        })
      }
    })
  },
})
