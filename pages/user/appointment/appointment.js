// pages/user/appointment/appointment.js
var util = require('../../../utils/util.js');
var app = getApp();
var time_new= [];
var time_list_change= [];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    names: '',
    phone: '',
    address: '',
    address_detail: '',
    now_time: '', //开始年月
    last_time: '', //结束年月
    order_id: '', //订单编号
    date: '', //安装日期
    time_list: [],
    time_index: '',
    choose_id: '',
    limit:'',//时间限制
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      order_id: options.order_id
    })
    wx.showLoading({
      title: '加载中...',
    })
    that.getOrderDetail(); //获取订单详情
    that.getTimeshow(); //获取时间段
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  //选择安装日期
  bindDateChange: function(e) {
    //console.log(e.detail.value);
    var that = this;
    var date = e.detail.value;
    that.setData({
      date: date
    })
   
    var newCode = [];
    var time_list =time_list_change
     //如果时间限制处理时间段数组
     if (that.data.limit.is_later && date == that.data.limit.tomorrow_date){
       time_list.forEach(function (value, i) {
        if (util.isInArray(that.data.limit.time_id,value.time_id)){
          newCode.push(time_list[i].format);
        }
      })
    }else{
       time_list.forEach(function (value, i) {
           newCode.push(time_list[i].format);
       })
    }
      that.setData({
        time_list: newCode,
        time_index: ""
      })
  },
  //选择时间段
  chooseTimeChange(e) {
    var that = this;
    var time_index = e.detail.value;
    var choseid = time_new[time_index].time_id
    that.setData({
      time_index: time_index,
      choose_id: choseid
    })
  },
  //获取订单详情
  getOrderDetail() {
    var that = this;
    var order_id = that.data.order_id;
    app.ajax({
      url: 'User/Order/getOrderDetail',
      method: "POST",
      data: {
        order_id: order_id
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code = 1000) {
          var change_address = res.data.data.province + res.data.data.city + res.data.data.area;
          that.setData({
            names: res.data.data.contact,
            phone: res.data.data.contact_tel,
            address: change_address,
            address_detail: res.data.data.address,
          })
        } else {
          app.showToast(res.data.message)
        }
      }

    })
  },
  //获取订单详情
  getTimeshow() {
    var that = this;
    var order_id = that.data.order_id;
    app.ajax({
      url: 'Common/Common/getRangeTime',
      method: "POST",
      data: {
        order_id: order_id
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code = 1000) {
          var newCode = [];
          var getTimeArr = res.data.data.time;
          for (var i = 0; i < getTimeArr.length; i++) {
            newCode.push(getTimeArr[i].format);
          }
          var timebox = res.data.time;
          that.setData({
            now_time: res.data.data.date.start,
            last_time: res.data.data.date.end,
            time_list: newCode,
            limit:res.data.data.limit
          })
          time_list_change= res.data.data.time;
          time_new= res.data.data.time;
        } else {
          app.showToast(res.data.message)
        }
      }

    })
  },
  //申请安装
  appointment: function(e) {
    var that = this;
    console.log(e,'eeeeeeee');
//    app.saveFormId(e.detail.formId);
    var content = e.detail.value.content;
    var choose_id = that.data.choose_id;
    var begin_time = e.detail.value.begin_time;
    var content_tel = e.detail.value.content_tel;
    var remarks = e.detail.value.remarks;
    var address = e.detail.value.address;
    var address_detail = e.detail.value.address_detail;
    var numreg = /^[1][3,4,5,7,8,9][0-9]{9}$/; //手机号码正则     
    if (begin_time == '') {
      app.showToast("请选择预约上门日期", "none", 2000, function () { });
      return false;
    }
    if (choose_id == '') {
      app.showToast("请选择安装时间段", "none", 2000, function () { });
      return false;
    }
    if (content == '') {
      app.showToast("请填写姓名", "none", 2000, function () { });
      return false;
    }
    if (content_tel == '') {
      app.showToast("手机号码不能空", "none", 2000, function () { });
      return false;
    }
    if (numreg.test(content_tel) == false) {
      app.showToast('请输入正确的手机号码', "none", 2000, function () { });
      return false;
    }
    if (address == '') {
      app.showToast("所在地为空,请联系客服", "none", 3000, function () { });
      return false;
    }
    if (address_detail == '') {
      app.showToast("安装地址为空,请联系客服", "none", 3000, function () { });
      return false;
    }
   // app.saveFormId(e.detail.formId);
    app.ajax({
      url: "User/Work/newInstall",
      data: {
        appointment_date: begin_time,
        appointment_time_id: choose_id,
        contacts: content,
        contact_number: content_tel,
        order_id: that.data.order_id,
        remarks: remarks
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message, "none", 2000, function () { });
          // wx.navigateBack({
          //   delta: 1
          // })
          setTimeout(() => {
            wx.switchTab({
              url: '../userHome/userHome',
            })
          }, 2000);
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
  }
})  