// pages/versionList/versionList.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 20;
var load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//列表
    empty: false,//空数据
    hasMore: false,//是否还有更多
    loading: false,//加载更多
    load_show: false,//整体加一个开关 加载
    is_waiting: '',//初始化加载
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      is_waiting: true
    })
    PAGE = 1;
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    PAGE++;
    that.getList();
  },
  // 获取列表
  getList(){
    let that = this;
    app.ajax({
      url: 'Common/Version/getVersionList',
      method: "POST",
      load: load,
      msg: '加载中...',
      data:{
        'page': PAGE,
        'limit': PAGESIZE,
        'client':4
      },
      success(res){
        wx.hideLoading();
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            that.setData({
              list: res.data.data,
              empty: false
            });
          } else {
            that.setData({
              list: that.data.list.concat(res.data.data),
              empty: false
            });
          }
          var hasMore = true;
          if (res.data.data.length < PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        } else {
          if (PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          load_show: true
        })
      }
    })
  },
  // 跳转详情
  goDetail(e){
    wx.navigateTo({
      url: '../detail/detail?id='+e.currentTarget.dataset.id+'&title=版本更新说明',
    })
  }
})